/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import com.jogamp.opengl.GL2;
import java.awt.Color;
import java.awt.Graphics;

public class CirculoLleno extends Forma{
    
    public CirculoLleno(float lados, float grados, float x, float y, float radio, float r, float g, float b){
        this.NLados = lados;
        this.grados = grados;
        this.x = x;
        this.y = y;
        this.radio = 30;
        xs = 1;
        ys = 1;
        this.r = r;
        this.g = g;
        this.b = b;
    }
    
    public void dibujaFigura(GL2 gl){
        
        gl.glLoadIdentity();
        
        gl.glTranslatef(x, y, 0);
        gl.glRotatef(grados, 0, 0, 1);
        gl.glScalef(xs, ys, 0f);
        

        gl.glBegin(GL2.GL_POLYGON);
        gl.glColor3f(r, g, b);
        
        float x1,y1,x2,y2;
        
        float inc = 2;
        
        x1 = (float)(radio*Math.sin(Math.toRadians(0)));
        y1 = (float)(radio*Math.cos(Math.toRadians(0)));
        
        for(float i=0;i<=180;i+=inc){
            x2 = (float)(radio*Math.sin(Math.toRadians(i)));
            y2 = (float)(radio*Math.cos(Math.toRadians(i)));
            gl.glVertex3f(x2,y2,0);
        }
        gl.glVertex3f(x1,y1,0);
        
        for(float i=180;i<=360;i+=inc){
            x2 = (float)(radio*Math.sin(Math.toRadians(i)));
            y2 = (float)(radio*Math.cos(Math.toRadians(i)));
            gl.glVertex3f(x2,y2,0);
        }
        gl.glEnd();
    }

    
    public boolean checa(int xe, int ye) {
        float dist = (float)(Math.sqrt((xe-x)*(xe-x)+(ye-y)*(ye-y)));
        if(dist<=radio)
            return true;
        return false;
    }
    
}
