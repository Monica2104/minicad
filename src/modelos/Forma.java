/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import com.jogamp.opengl.GL2;
import java.awt.Color;
import java.awt.Graphics;

public abstract class Forma{
    
    float r,g,b;
    float xs,ys,incs;
    float grados;
    float NLados;
    float x,y,radio;
    float incGrad;
    
    public void dibujaFigura(GL2 gl){
        
    }
    
    public boolean checa(int x, int y) {
        return true;
    }
    
    public void setX(float x) {
        this.x = x;
    }
    
    public void setY(float y) {
        this.y = y;
    }
    
    public void setXS(float xs){
        this.xs = xs;
    }
    
    public void setYS(float ys){
        this.ys = ys;
    }
    
    public float getXS() {
        return this.xs;
    }
    
    public float getYS(){
        return this.ys;
    }
    
    public void cambiaColor(float r, float g, float b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }
    
    public float getGrados(){
        return this.grados;
    }
    
    public void setGrados(float grados){
        this.grados = grados;
    }
    
}
