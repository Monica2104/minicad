/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import com.jogamp.opengl.GL2;
import java.awt.Color;
import java.awt.Graphics;

public class PentagonoVacio extends Forma{
    
    public PentagonoVacio(float lados, float grados, float x, float y, float radio, float r, float g, float b){
        this.NLados = lados;
        this.grados = grados;
        this.x = x;
        this.y = y;
        this.radio = 30;
        xs = 1;
        ys = 1;
        this.r = r;
        this.g = g;
        this.b = b;
    }
    
    
    public void dibujaFigura(GL2 gl) {
        
        gl.glLoadIdentity();
        
        gl.glTranslatef(x, y, 0);
        gl.glRotatef(grados, 0, 0, 1);
        gl.glScalef(xs, ys, 0f);

        gl.glBegin(GL2.GL_LINES);
        gl.glColor3f(r, g, b);
        
        float x1,y1,x2,y2,x3,y3,x4,y4;
        
        float inc = 72;
        float gr = -36;
        
        float psx[] = new float[5];
        float psy[] = new float[5];
        
        for(float i=0;i<5;i++) {
            psx[(int)i] = (float)(radio*Math.sin(Math.toRadians(gr)));
            psy[(int)i] = (float)(radio*Math.cos(Math.toRadians(gr)));
            gr+=inc;
        }
        
        for(float i=1;i<5;i++) {
            gl.glVertex3f(psx[(int)i-1], psy[(int)i-1], 0);
            gl.glVertex3f(psx[(int)i], psy[(int)i], 0);
        }
        gl.glVertex3f(psx[0], psy[0], 0);
        gl.glVertex3f(psx[4], psy[4], 0);
        
        gl.glEnd();
    }
    
    public boolean checa(int xe, int ye) {
        float dist = (float)(Math.sqrt((xe-x)*(xe-x)+(ye-y)*(ye-y)));
        if(dist<=radio)
            return true;
        return false;
    }

}

