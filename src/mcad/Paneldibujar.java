/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mcad;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.awt.GLCanvas;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JPanel;
import modelos.CirculoVac;
import modelos.Forma;

public class Paneldibujar extends GLCanvas{
    
    public ArrayList<Forma> formas;
    public Forma forma;
    GLCapabilities capabilities;
    
    public Paneldibujar(GLCapabilities capabilities){
        formas = new ArrayList<Forma>();
        this.capabilities = capabilities;
    }
    
    public void addEventos(Controlador c){
        this.addKeyListener(c);
        this.addMouseListener(c);
        
    }
    
    
}
