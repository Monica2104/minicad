/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mcad;

import com.jogamp.opengl.GLCapabilities;
import java.awt.BorderLayout;
import java.awt.Color;
import static java.awt.Color.pink;
import java.awt.Graphics;
import java.awt.GridLayout;
import javax.swing.*;

public class Panel extends JPanel{
    
    JPanel panelbotones;
    JButton bcirculolleno,bcuadradolleno,bpentagonolleno,brombolleno,btriangulolleno;
    JButton bcirculovacio,bcuadradovacio,bpentagonovacio,brombovacio,btriangulovacio;
    JButton brotarizq,brotarder,baumentarx,breducirx,baumentary,breduciry;
    JButton bborrar;
    Paneldibujar Paneldibujar;
    GLCapabilities capabilities;
    
    public int selected;
    
    public Panel(GLCapabilities capabilities){
        this.capabilities = capabilities;
        init();
    }
    
    public void init() {
        this.setLayout(new BorderLayout());
        
        panelbotones = new JPanel();
        panelbotones.setLayout(new GridLayout(2, 9, 2, 2));
        panelbotones.setBackground(pink);
        
        Paneldibujar = new Paneldibujar(capabilities);
        Paneldibujar.setSize(800, 600);
        
        bcirculolleno = new JButton("Circ LLeno");
        bcirculolleno.setName("CirL");
        bcirculolleno.setBackground(pink);
        bcirculovacio = new JButton("Circ Vac");
        bcirculovacio.setName("CirV");
        bcirculovacio.setBackground(pink);
        
        
        bcuadradolleno = new JButton("Cuad LLeno");
        bcuadradolleno.setName("CL");
        bcuadradolleno.setBackground(pink);
        bcuadradovacio = new JButton("Cuad Vac");
        bcuadradovacio.setName("CV");
        bcuadradovacio.setBackground(pink);
        
        bpentagonolleno = new JButton("Pentagono LLeno");
        bpentagonolleno.setName("PL");
        bpentagonolleno.setBackground(pink);
        bpentagonovacio = new JButton("Pentagono Vac");
        bpentagonovacio.setName("PV");
        bpentagonovacio.setBackground(pink);
        
        btriangulolleno = new JButton("Tri LLeno");
        btriangulolleno.setName("TL");
        btriangulolleno.setBackground(pink);
        btriangulovacio = new JButton("Tri Vac");
        btriangulovacio.setName("TV");
        btriangulovacio.setBackground(pink);
        
        brombolleno = new JButton("Rombo LLeno");
        brombolleno.setName("RL"); 
        brombolleno.setBackground(pink);
        brombovacio = new JButton("Rombo Vac");
        brombovacio.setName("RV");
        brombovacio.setBackground(pink);
        
        brotarizq = new JButton("Rotar izq");
        brotarizq.setName("RIzq");
        brotarizq.setBackground(pink);
        brotarder = new JButton("Rotar der");
        brotarder.setName("RDer");
        brotarder.setBackground(pink);
        
        baumentarx = new JButton("Aumentar X ");
        baumentarx.setName("AX");
        baumentarx.setBackground(pink);
        breducirx = new JButton("Reducir X ");
        breducirx.setName("RX");
        breducirx.setBackground(pink);
        
        baumentary = new JButton("Aumentar Y ");
        baumentary.setName("AY");
        baumentary.setBackground(pink);
        breduciry = new JButton("Reducir Y ");
        breduciry.setName("RY");
        breduciry.setBackground(pink);
        
        bborrar = new JButton("Borrar figura");
        bborrar.setName("borrar");
        bborrar.setBackground(pink);
        
        
        panelbotones.add(bcirculolleno);
        panelbotones.add(bcirculovacio);
        panelbotones.add(bcuadradolleno);
        panelbotones.add(bcuadradovacio);
        panelbotones.add(bpentagonolleno);
        panelbotones.add(bpentagonovacio);
        panelbotones.add(brombolleno);
        panelbotones.add(brombovacio);
        panelbotones.add(btriangulolleno);
        panelbotones.add(btriangulovacio);
        panelbotones.add(brotarizq);
        panelbotones.add(brotarder);
        panelbotones.add(baumentarx);
        panelbotones.add(breducirx);
        panelbotones.add(baumentary);
        panelbotones.add(breduciry);
        panelbotones.add(bborrar);
        
        Paneldibujar.setBackground(Color.cyan);
        this.add(panelbotones,BorderLayout.NORTH);
        this.add(Paneldibujar,BorderLayout.CENTER);
        
    }
    
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
    }
    
    public void addEventos(Controlador c) {
        Paneldibujar.addMouseMotionListener(c);
        Paneldibujar.addMouseListener(c);
        this.Paneldibujar.addEventos(c);
        
        bcirculovacio.addActionListener(c);
        bcirculolleno.addActionListener(c);
        bcuadradovacio.addActionListener(c);
        bcuadradolleno.addActionListener(c);
        bpentagonovacio.addActionListener(c);
        bpentagonolleno.addActionListener(c);
        btriangulovacio.addActionListener(c);
        btriangulolleno.addActionListener(c);
        brombovacio.addActionListener(c);
        brombolleno.addActionListener(c);
        brotarizq.addActionListener(c);
        brotarder.addActionListener(c);
        baumentarx.addActionListener(c);
        breducirx.addActionListener(c);
        baumentary.addActionListener(c);
        breduciry.addActionListener(c);
        bborrar.addActionListener(c);
    }
}


