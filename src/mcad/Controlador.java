/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mcad;

import java.awt.event.*;
import javax.swing.JComponent;
import modelos.*;

public class Controlador implements ActionListener, MouseMotionListener, MouseListener, KeyListener {

    Panel panel;

    public Controlador(Panel panel) {
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JComponent c = (JComponent) e.getSource();
        String opt = c.getName();
        int ex, ey;
        float rotarGrados;
        float escalarX, escalarY;
        float r,g,b;
        
        float v = (float)(Math.random()*100);
        r = ((int)v)%2;
        
        v = (float)(Math.random()*100);
        g = ((int)v)%2;
        
        v = (float)(Math.random()*100);
        b = ((int)v)%2;
        
        // si los 3 son igual a 1 eso quiere decir
        // se dibujara en blanco y como el fondo es blanco no se va a ver
        // asi que se dibuja en negro
        if(r==1 && g==1 && b==1){
            r = 0;
            g = 0;
            b = 0;
        }

        switch (opt) {
            case "CirL":
                panel.Paneldibujar.forma = new CirculoLleno(180, 0, 100, 100, 30,r,g,b);
                panel.Paneldibujar.formas.add(panel.Paneldibujar.forma);
                break;
            case "CirV":
                panel.Paneldibujar.forma = new CirculoVac(180, 0, 100, 100, 30,r,g,b);
                panel.Paneldibujar.formas.add(panel.Paneldibujar.forma);
                break;
            case "CL":
                panel.Paneldibujar.forma = new CuadradoLleno(4, 0, 100, 100, 30,r,g,b);
                panel.Paneldibujar.formas.add(panel.Paneldibujar.forma);
                break;
            case "CV":
                panel.Paneldibujar.forma = new CuadradoVacio(4, 0, 100, 100, 30,r,g,b);
                panel.Paneldibujar.formas.add(panel.Paneldibujar.forma);
                
                break;
            case "PL":
                panel.Paneldibujar.forma = new PentagonoLleno(4, 0, 100, 100, 30,r,g,b);
                panel.Paneldibujar.formas.add(panel.Paneldibujar.forma);
                break;
            case "PV":
                panel.Paneldibujar.forma = new PentagonoVacio(5, 0, 100, 100, 30,r,g,b);
                panel.Paneldibujar.formas.add(panel.Paneldibujar.forma);
                break;
            case "TL":
                panel.Paneldibujar.forma = new TrianguloLleno(3, 0, 100, 100, 30,r,g,b);
                panel.Paneldibujar.formas.add(panel.Paneldibujar.forma);
                break;
            case "TV":
                panel.Paneldibujar.forma = new TrianguloVacio(3, 0, 100, 100, 30,r,g,b);
                panel.Paneldibujar.formas.add(panel.Paneldibujar.forma);
                break;
            case "RL":
                panel.Paneldibujar.forma = new RomboLleno(4, 0, 100, 100, 30,r,g,b);
                panel.Paneldibujar.formas.add(panel.Paneldibujar.forma);
                break;
            case "RV":
                panel.Paneldibujar.forma = new RomboVacio(4, 0, 100, 100, 30,r,g,b);
                panel.Paneldibujar.formas.add(panel.Paneldibujar.forma);
                break;
            case "RIzq":
                rotarGrados = panel.Paneldibujar.formas.get(panel.selected).getGrados();
                panel.Paneldibujar.formas.get(panel.selected).setGrados(rotarGrados - 5);
                break;
            case "RDer":
                rotarGrados = panel.Paneldibujar.formas.get(panel.selected).getGrados();
                panel.Paneldibujar.formas.get(panel.selected).setGrados(rotarGrados + 5);
                break;
            case "AX":
                escalarX = panel.Paneldibujar.formas.get(panel.selected).getXS();
                panel.Paneldibujar.formas.get(panel.selected).setXS(escalarX + 1);
                break;
            case "RX":
                escalarX = panel.Paneldibujar.formas.get(panel.selected).getXS();
                
                if (escalarX > 1) {
                    panel.Paneldibujar.formas.get(panel.selected).setXS(escalarX - 1);
                }
                break;
            case "AY":
                escalarY = panel.Paneldibujar.formas.get(panel.selected).getYS();
                panel.Paneldibujar.formas.get(panel.selected).setYS(escalarY + 1);
                break;
            case "RY":

                escalarY = panel.Paneldibujar.formas.get(panel.selected).getYS();
                if (escalarY > 1) {
                    panel.Paneldibujar.formas.get(panel.selected).setYS(escalarY - 1);
                }

                break;
            case "borrar":
                panel.Paneldibujar.formas.remove(panel.selected);
                break;
        }

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        int xe = e.getX();
        int ye = e.getY();
        panel.Paneldibujar.formas.get(panel.selected).setX(xe);
        panel.Paneldibujar.formas.get(panel.selected).setY(ye);
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int xe = e.getX();
        int ye = e.getY();

        for (int i = 0; i < panel.Paneldibujar.formas.size(); i++) {
            if (panel.Paneldibujar.formas.get(i).checa(xe, ye)) {
                System.out.println("le diste clic a la figura!!!");
                panel.selected = i;
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {
        
        switch (e.getKeyChar()) {
            case 'n':
                panel.Paneldibujar.formas.get(panel.selected).cambiaColor(0, 0, 0);
                break;
            case 'r':
                panel.Paneldibujar.formas.get(panel.selected).cambiaColor(1, 0, 0);
                break;
            case 'v':
                panel.Paneldibujar.formas.get(panel.selected).cambiaColor(0, 1, 0);
                break;
            case 'a':
                panel.Paneldibujar.formas.get(panel.selected).cambiaColor(0, 0, 1);
                break;
            case 'y':
                panel.Paneldibujar.formas.get(panel.selected).cambiaColor(1, 1, 0);
                break;
            case 'm':
                panel.Paneldibujar.formas.get(panel.selected).cambiaColor(1, 0, 1);
                break;
            case 'c':
                panel.Paneldibujar.formas.get(panel.selected).cambiaColor(0, 1, 1);
                break;
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }

}
