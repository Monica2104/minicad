/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mcad;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.Animator;
import javax.swing.*;

public class Mcad implements GLEventListener{
    /**
     * @param args the command line arguments
     */
    Panel panel;
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        final GLProfile profile = GLProfile.get(GLProfile.GL2);
        GLCapabilities capabilities = new GLCapabilities(profile);
        
        Mcad graf = new Mcad();
        
        JFrame frame = new JFrame("MiniCad");
        
        graf.panel = new Panel(capabilities);
        
        graf.panel.Paneldibujar.addGLEventListener(graf);
        
        Controlador controlador = new Controlador(graf.panel);
        graf.panel.addEventos(controlador);
        
        Animator animator = new Animator(graf.panel.Paneldibujar);
        
        frame.add(graf.panel);
        
        frame.setSize(1200,600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
        animator.start();
    }

    @Override
    public void init(GLAutoDrawable glad) {
        GL2 gl = glad.getGL().getGL2();
        
        gl.glMatrixMode(GL2.GL_PROJECTION); 
        gl.glLoadIdentity(); 
        //glu.gluPerspective(45.0f, h, 1.0, 20.0); 
        
        gl.glOrtho(0.0, 800.0, 600.0, 0.0, 0, 1.0);
        
        gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    }

    @Override
    public void dispose(GLAutoDrawable glad) {
        
    }

    @Override
    public void display(GLAutoDrawable glad) {
        final GL2 gl = glad.getGL().getGL2();
        
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
        
        for(int i=0;i<panel.Paneldibujar.formas.size();i++){
            gl.glPushMatrix();
            panel.Paneldibujar.formas.get(i).dibujaFigura(gl);
            gl.glPopMatrix();
        }
        
        
        
        gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        gl.glFlush();
    }

    @Override
    public void reshape(GLAutoDrawable glad, int x, int y, int width, int height) {
        final GL2 gl = glad.getGL().getGL2();  

        // get the OpenGL 2 graphics object   
        if(height <= 0) height = 1; 

        //preventing devided by 0 exception height = 1; 
        final float h = (float) width / (float) height; 

        // display area to cover the entire window 
        gl.glViewport(0, 0, 800, 600);         
        //transforming projection matrix 
        gl.glMatrixMode(GL2.GL_PROJECTION); 
        gl.glLoadIdentity(); 
        //glu.gluPerspective(45.0f, h, 1.0, 20.0); 
        
        gl.glOrtho(0.0, 800.0, 600.0, 0.0, 0, 1.0);
        

        //transforming model view gl.glLoadIdentity(); 
        gl.glMatrixMode(GL2.GL_MODELVIEW); 
        gl.glLoadIdentity(); 
    }
    
}
